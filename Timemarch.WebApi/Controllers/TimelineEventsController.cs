﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Timemarch.DataModel;
using Timemarch.DataModel.Models;
using Timemarch.DataModel.Repositories;

namespace Timemarch.WebApi.Controllers
{
    public class TimelineEventsController : ApiController
    {
        private TimemarchContext db = new TimemarchContext();
        private readonly TimelineEventsRepository _repo = new TimelineEventsRepository();



        [Route("EventsByTimeline")]
        [HttpGet]
        //EventsByTimeline?timelineid=1
        public IHttpActionResult GetEventsForTimeline(int timelineId)
        {
            try
            {
                return Ok(_repo.GetTimelineEvents()
                    .Where(r=> r.TimelineId == timelineId)
                    .AsQueryable()

                    );
            }
            catch (Exception)
            {

                return InternalServerError();//500
            }

        }
        // GET: api/TimelineEvents
        public IHttpActionResult GetTimelineEvents()
        {
            //return db.TimelineEvents;
            try
            {
                return Ok(_repo.GetTimelineEvents().AsQueryable());
            }
            catch (Exception)
            {

                return InternalServerError();//500
            }
        }

        // GET: api/TimelineEvents/5
        [ResponseType(typeof(TimelineEvent))]
        public IHttpActionResult GetTimelineEvent(int id)
        {
            TimelineEvent timelineEvent = _repo.GetTimelineEventById(id);
            if (timelineEvent == null)
            {
                return NotFound();
            }

            return Ok(timelineEvent);
        }

        // PUT: api/TimelineEvents/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTimelineEvent(int id, TimelineEvent timelineEvent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != timelineEvent.Id)
            {
                return BadRequest();
            }

            //db.Entry(timelineEvent).State = EntityState.Modified;

            try
            {
                _repo.UpdateTimelineEvent(timelineEvent);
                //db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_repo.TimelineEventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TimelineEvents
        [ResponseType(typeof(TimelineEvent))]
        public IHttpActionResult PostTimelineEvent(TimelineEvent timelineEvent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //db.TimelineEvents.Add(timelineEvent);
            //db.SaveChanges();
            _repo.AddTimelineEvent(timelineEvent);

            return CreatedAtRoute("DefaultApi", new { id = timelineEvent.Id }, timelineEvent);
        }

        // DELETE: api/TimelineEvents/5
        [ResponseType(typeof(TimelineEvent))]
        public IHttpActionResult DeleteTimelineEvent(int id)
        {
            _repo.DeleteTimelineEvent(id);
            return Ok(id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

       

       
    }//
}//