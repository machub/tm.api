﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Timemarch.DataModel;
using Timemarch.DataModel.Models;
using Timemarch.DataModel.Repositories;

namespace Timemarch.WebApi.Controllers
{

   
    public class TimelinesController : ApiController
    {
        //private TimemarchContext db = new TimemarchContext();
        private readonly  TimelineRepository _repo = new TimelineRepository();

        // GET: api/Timelines
        [ResponseType(typeof(Timeline))] //for help documentation
        public IHttpActionResult GetTimelines()
        {
           // return db.Timelines;
            try
            {
                return Ok(_repo.GetTimelines().AsQueryable());
            }
            catch (Exception)
            {
                
                return InternalServerError();//500
            }
        }

        // GET: api/Timelines/5
        [ResponseType(typeof(Timeline))]
        public IHttpActionResult GetTimeline(int id)
        {
            //Timeline timeline = db.Timelines.Find(id);
            Timeline timeline = _repo.GetTimelineById(id);
            if (timeline == null)
            {
                return NotFound();
            }

            return Ok(timeline);
        }

        // PUT: api/Timelines/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTimeline(int id, [FromBody] Timeline timeline)
        {
            if (timeline == null)
            {
                return BadRequest("Timeline cannot be nil");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != timeline.Id)
            {
                return BadRequest();
            }

           // db.Entry(timeline).State = EntityState.Modified;

            try
            {
                //db.SaveChanges();
                _repo.UpdateTimeline(timeline);
                
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_repo.TimelineExists(id))
                {
                    return NotFound();
                }
                else
                {
                    // throw;
                    return InternalServerError(); //500 error
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Timelines
        [ResponseType(typeof(Timeline))]
        public IHttpActionResult PostTimeline([FromBody]Timeline timeline)
        {
            if (timeline == null)
            {
                return BadRequest("Product cannot be nil");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            //db.Timelines.Add(timeline);
            //db.SaveChanges();
            _repo.AddTimeline(timeline);

            // return Created<Product>(Request.RequestUri + newProduct.ProductId.ToString(), newProduct);
            return CreatedAtRoute("DefaultApi", new { id = timeline.Id }, timeline);
        }

        // DELETE: api/Timelines/5
        [ResponseType(typeof(Timeline))]
        public IHttpActionResult DeleteTimeline(int id)
        {
            //Timeline timeline = db.Timelines.Find(id);
            //if (timeline == null)
            //{
            //    return NotFound();
            //}

            //db.Timelines.Remove(timeline);
            //db.SaveChanges();

            //return Ok(timeline);
            _repo.DeleteTimeline(id);
            return Ok(id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
               // db.Dispose();
            }
            base.Dispose(disposing);
        }

        //private bool TimelineExists(int id)
        //{
        //    return db.Timelines.Count(e => e.Id == id) > 0;
        //}
    }
}