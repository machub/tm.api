﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Timemarch.DataModel;
using Timemarch.DataModel.Models;

namespace Timemarch.WebApi.Tests.dbTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CreateTimeline()
        {
            var t = new Timeline {Title = "First Timeline", UserId = 1 };
            using (var  context = new TimemarchContext())
            {
                context.Timelines.Add(t);
                context.SaveChanges();
            }
        }
    }
}
