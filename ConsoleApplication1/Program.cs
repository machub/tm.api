﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timemarch.DataModel;
using Timemarch.DataModel.Models;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            //stop database initializing
            Database.SetInitializer(new NullDatabaseInitializer<TimemarchContext>());
            //CreateTimeline();
            //CreateEvents();
            //IncludeGraphQuery();
            //ExplicitLoadQuery();
            ProjectionQuery();

            Console.ReadKey();
        }

        private static void CreateEvents()
        {
            using (var context = new TimemarchContext())
            {
                context.Database.Log = Console.WriteLine;

                Timeline  firstTimeline = context.Timelines.First();

                var e = new TimelineEvent();
                e.DisplayFlag = 0;
                e.FromDate = DateTime.Now;
                e.ToDate = DateTime.Now;
                e.PeriodDisplay = "2015";
                e.Summary = "This is second summary";
                e.TimelineId = firstTimeline.Id;
                e.Timeline = firstTimeline;


                var e1 = new TimelineEvent();
                e1.DisplayFlag = 0;
                e1.FromDate = DateTime.Now;
                e1.ToDate = DateTime.Now;
                e1.PeriodDisplay = "2016";
                e1.Summary = "This is third summary";
                e1.TimelineId = firstTimeline.Id;
                e.Timeline = firstTimeline;

                context.TimelineEvents.Add(e);
                context.TimelineEvents.Add(e1);
                context.SaveChanges();

            }
        }
         
        static void CreateTimeline()
        {
            var t = new Timeline { Title = "Second Timeline", UserId = 1 };
            try
            {
                using (var context = new TimemarchContext())
                {
                    context.Database.Log = Console.WriteLine;
                    context.Timelines.Add(t);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var e = ex.InnerException;

            }
            
          
        }

        //------Graph Queries---------//

        //include
        //Eager load using include
        //performance degrades as you add more eage load collections
        static void IncludeGraphQuery()
        {
            using (var context = new TimemarchContext())
            {
                context.Database.Log = Console.WriteLine;
                var timeline = context.Timelines.Include(t=> t.TimelineEvents).First();
                Console.WriteLine(timeline.Title);
            }
        }

        //performs a simple select : better performance
        static void ExplicitLoadQuery()
        {
            using (var context = new TimemarchContext())
            {
                context.Database.Log = Console.WriteLine;
                var timeline = context.Timelines.First();
                Console.WriteLine(timeline.Title);
                context.Entry(timeline).Collection(t=> t.TimelineEvents).Load();
                //Entry gets the context timeline from graph
                //collection gets the navigation item i.e child item
                //load loads it
            }
        }

        //Lazy loading : not recommended as can be misused
        // In your model you will mark the navigation property as virtual
        // ig. public virtual List<TimelineEvent> TimelineEvents { get; set; }
        // now when navigation data is required, it will automatically be loaded
        //  var timeline = context.Timelines.First();
        //  var eventCnt = timeline.TimelineEvents.Count();
        //last row will trigger load of child data


        //Projections

        static void ProjectionQuery()
        {
            using (var context = new TimemarchContext())
            {
                context.Database.Log = Console.WriteLine;
                var timelines = context.Timelines
                    .Select(n => new {n.Title, n.TimelineEvents})
                    .ToList();

            }
        }

    }//class
}//ns
