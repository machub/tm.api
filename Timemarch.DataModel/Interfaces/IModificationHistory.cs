﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timemarch.DataModel.Interfaces
{
    public interface IModificationHistory
    {
         DateTime CreatedDateTime { get; set; }
         DateTime ModifiedDateTime { get; set; }
         bool IsDirty { get; set; }
    
    }
}
