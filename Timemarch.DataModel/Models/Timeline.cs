﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Timemarch.DataModel.Interfaces;

namespace Timemarch.DataModel.Models
{
    public class Timeline : IModificationHistory
    {
        public Timeline()
        {
            TimelineEvents = new List<TimelineEvent>();
        }
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Title { get;set;}
        public List<TimelineEvent> TimelineEvents { get; set; }
        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDateTime { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public bool IsDirty { get; set; }
    }
}