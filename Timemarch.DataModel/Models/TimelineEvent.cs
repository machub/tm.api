﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Timemarch.DataModel.Interfaces;

namespace Timemarch.DataModel.Models
{
    public class TimelineEvent : IModificationHistory
    {
        public int Id { get; set; }
        public int TimelineId { get; set; } //foreign key
        public Timeline Timeline { get; set; } //navigation property
        public string Summary { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int DisplayFlag { get; set; }
        public string PeriodDisplay { get; set; }
        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDateTime { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public bool IsDirty { get; set; }
    }
}