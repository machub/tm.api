﻿using System;
using System.Data.Entity;
using System.Linq;
using Timemarch.DataModel.Interfaces;
using Timemarch.DataModel.Models;

namespace Timemarch.DataModel
{
    public class TimemarchContext : DbContext
    {
        public DbSet<Timeline> Timelines { get; set; }
        public DbSet<TimelineEvent> TimelineEvents { get; set; }

        public TimemarchContext() : base("DefaultConnection")
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Types().Configure(c => c.Ignore("IsDirty"));
            base.OnModelCreating(modelBuilder);
        }

        //On save we want to update create 
        public override int SaveChanges()
        {
            //get each class that implements IModifiedHistory & is added or modified
            foreach (var history in this.ChangeTracker.Entries()
              .Where(e => e.Entity is IModificationHistory && (e.State == EntityState.Added ||
                      e.State == EntityState.Modified))
               .Select(e => e.Entity as IModificationHistory)
              )
            {
                history.ModifiedDateTime = DateTime.Now;

                //new items
                if (history.CreatedDateTime == DateTime.MinValue)
                {
                    history.CreatedDateTime = DateTime.Now;
                }
            }
            int result = base.SaveChanges();

            //reset the isDirty flag
            foreach (var history in this.ChangeTracker.Entries()
                                          .Where(e => e.Entity is IModificationHistory)
                                          .Select(e => e.Entity as IModificationHistory)
              )
            {
                history.IsDirty = false;
            }
            return result;
        }
    }//
}//
