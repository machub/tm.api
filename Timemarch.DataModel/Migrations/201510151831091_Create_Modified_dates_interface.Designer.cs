// <auto-generated />
namespace Timemarch.DataModel.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Create_Modified_dates_interface : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Create_Modified_dates_interface));
        
        string IMigrationMetadata.Id
        {
            get { return "201510151831091_Create_Modified_dates_interface"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
