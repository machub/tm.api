namespace Timemarch.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_auto_create_date : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TimelineEvents", "CreatedDateTime", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.Timelines", "CreatedDateTime", c => c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Timelines", "CreatedDateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.TimelineEvents", "CreatedDateTime");
        }
    }
}
