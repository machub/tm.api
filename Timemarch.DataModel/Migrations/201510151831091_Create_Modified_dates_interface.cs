namespace Timemarch.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create_Modified_dates_interface : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TimelineEvents", "ModifiedDateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.Timelines", "ModifiedDateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Timelines", "ModifiedDateTime");
            DropColumn("dbo.TimelineEvents", "ModifiedDateTime");
        }
    }
}
