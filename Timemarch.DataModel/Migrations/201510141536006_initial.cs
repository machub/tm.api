namespace Timemarch.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TimelineEvents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimelineId = c.Int(nullable: false),
                        Summary = c.String(),
                        Fromdate = c.DateTime(nullable: false),
                        ToDate = c.DateTime(nullable: false),
                        DisplayFlag = c.Int(nullable: false),
                        PeriodDisplay = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Timelines", t => t.TimelineId, cascadeDelete: true)
                .Index(t => t.TimelineId);
            
            CreateTable(
                "dbo.Timelines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TimelineEvents", "TimelineId", "dbo.Timelines");
            DropIndex("dbo.TimelineEvents", new[] { "TimelineId" });
            DropTable("dbo.Timelines");
            DropTable("dbo.TimelineEvents");
        }
    }
}
