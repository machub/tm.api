namespace Timemarch.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_create_date_timeline : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Timelines", "CreatedDateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Timelines", "CreatedDateTime");
        }
    }
}
