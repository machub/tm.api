﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Timemarch.DataModel.Models;

namespace Timemarch.DataModel.Repositories
{
    public class TimelineRepository
    {
        public List<TimelineView> GetTimelines()
        {
            using (var context = new TimemarchContext())
            {
                return context.Timelines.AsNoTracking()
                    .Select(n => new TimelineView {
                        Id = n.Id,
                        Title = n.Title,
                        UserName = "testUser",
                        CreatedDateTime = n.CreatedDateTime,
                        ModifiedDateTime = n.ModifiedDateTime
                    })
                    
                    .ToList();
            }
        }
        public Timeline GetTimelineById(int id)
        {
            using (var context = new TimemarchContext())
            {
               // return context.Timelines.Find(id);
                 return context.Timelines.AsNoTracking().SingleOrDefault(n => n.Id == id);
            }
        }

        public void AddTimeline(Timeline timeline)
        {
            using (var context = new TimemarchContext())
            {
                context.Timelines.Add(timeline);
                context.SaveChanges();

            }
        }

       
        public void UpdateTimeline(Timeline t)
        {
            using (var context = new TimemarchContext())
            {
                context.Entry(t).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteTimeline(int id)
        {
            using (var context = new TimemarchContext())
            {
                var t = context.Timelines.Find(id);
                context.Entry(t).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }
        public List<TimelineEvent> GetTimelineEvents(int timelineId)
        {
            using (var context = new TimemarchContext())
            {
                return context.TimelineEvents.AsNoTracking().Where(r=> r.TimelineId == timelineId).ToList();
            }
        }

        public bool TimelineExists(int id)
        {
            using (var context = new TimemarchContext())
            {
                return context.Timelines.Count(e => e.Id == id) > 0;
            }
           
        }
    }//
}//
