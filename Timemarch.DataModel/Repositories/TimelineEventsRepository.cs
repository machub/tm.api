﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timemarch.DataModel.Models;

namespace Timemarch.DataModel.Repositories
{
    public class TimelineEventsRepository
    {
        public List<TimelineEvent> GetTimelineEvents()
        {
            using (var context = new TimemarchContext())
            {
                return context.TimelineEvents.AsNoTracking().ToList();
            }
        }
        public TimelineEvent GetTimelineEventById(int id)
        {
            using (var context = new TimemarchContext())
            {
                // return context.Timelines.Find(id);
                return context.TimelineEvents.AsNoTracking().SingleOrDefault(n => n.Id == id);
            }
        }

        public void AddTimelineEvent(TimelineEvent timelineEvent)
        {
            using (var context = new TimemarchContext())
            {
                context.TimelineEvents.Add(timelineEvent);
                context.SaveChanges();

            }
        }


        public void UpdateTimelineEvent(TimelineEvent t)
        {
            using (var context = new TimemarchContext())
            {
                context.Entry(t).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteTimelineEvent(int id)
        {
            using (var context = new TimemarchContext())
            {
                var t = context.TimelineEvents.Find(id);
                context.Entry(t).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }
        public bool TimelineEventExists(int id)
        {
            using (var context = new TimemarchContext())
            {
                return context.TimelineEvents.Count(e => e.Id == id) > 0;
            }

        }
    }//
}//
